# FST Hardware repository

The FST Hardware repository features all PCB's developed since 2019/2020 by FST Lisboa formula student team, templates and other useful files for every one to use can be found in this repo. Addicionaly this repository [Wiki](https://gitlab.com/projectofst/hardware/-/wikis/home) holds a series of tutorials regarding PCB design.

## Branching

There are two types of branches:

* Master Branch (master),
* Development Branches,

#### Master Branch

The master branch only holds PCB's which have been completed, review and manufactured.

#### The Develop Branch


Development branches hold PCB's which are in development and other contribuitons to the repository which have not been accepted to the master branch yeat.

Branch naming:

Branch names should contain the location of the file in the repo, for example if you develop a sensor PCB this should within the sensors folder, after the / put the PCB project name, snake_case only. 

* sensors/tyre_pressure
* general/templates/sch_template
* bms/bms_master
* telemetry_shield

Commit naming:

Commit names should represent what has been done, try to isolate each modification with a commit. There are types of commits:

* feat "feature" - Added a new feature to the PCB or schematic, normal development,
* fix "fix" - Some issue or erro with the PCB/file has been corrected,
*docs "documents" - Documentation has been produced, e.g output job released, pdf, .xls and other file types.

Some examples are:

* feat: can_interface
* feat: pcb_shape
* fix: silk_screen_clerance
* doc: sch_pdf
* doc: output_job_release

Proposed commit syntax:

[WARNING] : Do not add history folder when working on a PCB, most of the folder size is the history folder!



```
type: representative message
```

## Others

To learn more about this project, read the repo [Wiki](https://gitlab.com/projectofst/hardware/-/wikis/home)!
Dont forget to use team templates, find them in General folder!

### Documentation

Some documentation regarding PCB's can be found in [https://projectofst.gitlab.io/software10e/](https://projectofst.gitlab.io/software10e/)