*PADS-LIBRARY-PART-TYPES-V9*

FA-T220-51E FAT22051E I ANA 11 1 0 0 0
TIMESTAMP 2019.10.16.20.58.19
"Manufacturer_Name" Ohmite
"Manufacturer_Part_Number" FA-T220-51E
"Mouser Part Number" 588-FA-T220-51E
"Mouser Price/Stock" https://www.mouser.com/Search/Refine.aspx?Keyword=588-FA-T220-51E
"RS Part Number" 8934948
"RS Price/Stock" http://uk.rs-online.com/web/p/products/8934948
"Allied_Number" 70024607
"Allied Price/Stock" https://www.alliedelec.com/ohmite-fa-t220-51e/70024607/
"Description" Heatsink, TBH25 Ohmite Resistor Series, TCH35 Ohmite Resistor Series, TEH100 Ohmite Resistor Series, TEH70 Ohmite
"Datasheet Link" https://componentsearchengine.com/Datasheets/1/FA-T220-51E.pdf
"Geometry.Height" 50.8mm
GATE 1 2 0
FA-T220-51E
1 0 U 1
2 0 U 2

*END*
*REMARK* SamacSys ECAD Model
1120279/110404/2.44/2/4/Hardware
